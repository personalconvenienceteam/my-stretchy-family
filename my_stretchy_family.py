
#-----Statement of Authorship----------------------------------------#
#
#  This is an individual assessment item.  By submitting this
#  code I agree that it represents my own work.  I am aware of
#  the University rule that a student must not act in a manner
#  which constitutes academic dishonesty as stated and explained
#  in QUT's Manual of Policies and Procedures, Section C/5.3
#  "Academic Integrity" and Section E/2.1 "Student Code of Conduct".
#
#    Student no: N9438157
#    Student name: Roderick Lenz
#
#  NB: Files submitted without a completed copy of this statement
#  will not be marked.  All files submitted will be subjected to
#  software plagiarism analysis using the MoSS system
#  (http://theory.stanford.edu/~aiken/moss/).
#
#--------------------------------------------------------------------#



#-----Assignment Description-----------------------------------------#
#
#  MY STRETCHY FAMILY
#
#  This assignment tests your skills at defining functions, processing
#  data stored in lists and performing the arithmetic calculations
#  necessary to display a complex visual image.  The incomplete
#  Python script below is missing a crucial function, "draw_portrait".
#  You are required to complete this function so that when the
#  program is run it produces a portrait of a stick figure family in
#  the style of the car window stickers that have become popular in
#  recent years, using data stored in a list to determine the
#  locations and heights of the figures.  See the instruction
#  sheet accompanying this file for full details.
#
#  Note that this assignment is in two parts, the second of which
#  will be released only just before the final deadline.  This
#  template file will be used for both parts and you will submit
#  only your final solution, whether or not you complete both
#  parts.
#
#--------------------------------------------------------------------#  



#-----Preamble-------------------------------------------------------#
#
# This section imports necessary functions and defines constant
# values used for drawing the background.  You should not change any
# of the code in this section.
#

# Import the functions needed to complete this assignment.  You
# should not need to import any other modules for your solution.

from turtle import *
from math import *

# Define constant values used in the main program that sets up
# the drawing canvas.  Do not change any of these values.

window_height = 550 # pixels
window_width = 900 # pixels
grass_height = 200 # pixels
grass_offset = -100 # pixels
location_width = 150 # pixels
num_locations = 5

#
#--------------------------------------------------------------------#



#-----Functions for Drawing the Background---------------------------#
#
# The functions in this section are called by the main program to
# draw the background and the locations where the individuals in the
# portrait are required to stand.  You should not change any of
# the code in this section.  Note that each of these functions
# leaves the turtle's pen up.
#


# Draw the grass as a big green rectangle
def draw_grass():
    
    penup()
    goto(-window_width / 2, grass_offset) # start at the bottom-left
    setheading(90) # face north
    fillcolor('pale green')
    begin_fill()
    forward(grass_height)
    right(90) # face east
    forward(window_width)
    right(90) # face south
    forward(grass_height)
    right(90) # face west
    forward(window_width)
    end_fill()


# Draw the locations where the individuals must stand
def draw_locations(locations_on = True):

    # Only draw the locations if the argument is True
    if locations_on:

        # Define a small gap at each end of each location
        gap_size = 5 # pixels
        location_width_less_gaps = location_width - (gap_size * 2) # pixels

        # Start at the far left, facing east
        penup()
        goto(-num_locations * location_width / 2, 0)
        setheading(0) 
  
        # Draw each location as a thick line with a gap at each end
        color('dark khaki')
        for location in range(num_locations):
            penup()
            forward(gap_size)
            pendown()
            width(5) # draw a thick line
            forward(location_width_less_gaps)
            width(1)
            penup()
            forward(gap_size)


# Draw the numeric labels on the locations
def draw_labels(labels_on = True):

    # Only draw the labels if the argument is True
    if labels_on:
    
        font_size = 16 # size of characters for the labels

        # Start in the middle of the left-hand location, facing east
        penup()
        goto(-((num_locations - 1) * location_width) / 2,
             -font_size * 2)
        setheading(0) 

        # Walk to the right, print the labels as we go
        color('dark khaki')
        for label in range(num_locations):
            write(label, font = ('Arial', font_size, 'bold'))
            forward(location_width)


# As a debugging aid, mark certain absolute coordinates on the canvas
def mark_coords(marks_on = True):

    # Only mark the coordinates if the argument is True
    if marks_on:

        # Mark the "home" coordinate
        home()
        width(1)
        color('black')
        dot(3)
        write('0, 0', font = ('Arial', 10, 'normal'))

        # Mark the centre point of each individual's location
        max_x = (num_locations - 1) * location_width / 2
        penup()
        for x_coord in range(-max_x, max_x + location_width, location_width):
            for y_coord in [0, 400]:
                goto(x_coord, y_coord)
                dot(3)
                write(str(x_coord) + ', ' + str(y_coord),
                      font = ('Arial', 10, 'normal'))
                
#
#--------------------------------------------------------------------#



#-----Test data------------------------------------------------------#
#
# These are the data sets you will use to test your code.
# Each of the data sets is a list specifying the positions for
# the people in the portrait:
#
# 1. The name of the individual, from 'Person A' to 'Person D' or 'Pet'
# 2. The place where that person/pet must stand, from location 0 to 4
# 3. How much to stretch the person/pet vertically, from 0.5 to 1.5
#    times their normal height
# 4. A mystery value, either '*' or '-', whose purpose will be
#    revealed only in the second part of the assignment
#
# Each data set does not necessarily include all people and sometimes
# they require the same person to be drawn more than once.  You
# can assume, however, that they never put more than one person in
# the same location.
#
# You may add additional data sets but you may not change any of the
# given data sets below.
#

# The following data set doesn't require drawing any people at
# all.  You may find it useful as a dummy argument when you
# first start developing your "draw_portrait" function.

portrait_00 = []

# The following data sets each draw just one of the individuals
# at their default height.

portrait_01 = [['Person A', 2, 1.0, '-']]

portrait_02 = [['Person B', 3, 1.0, '-']]

portrait_03 = [['Person C', 1, 1.0, '-']]

portrait_04 = [['Person D', 0, 1.0, '-']]

portrait_05 = [['Pet', 4, 1.0, '-']]

# The following data sets each draw just one of the individuals
# but multiple times and at different sizes.

portrait_06 = [['Person A', 3, 1.0, '-'],
               ['Person A', 1, 0.75, '-'],
               ['Person A', 2, 0.5, '-'],
               ['Person A', 4, 1.4, '-']]

portrait_07 = [['Person B', 0, 0.5, '-'],
               ['Person B', 2, 1.0, '-'],
               ['Person B', 3, 1.5, '-']]

portrait_08 = [['Person C', 0, 0.5, '-'],
               ['Person C', 1, 0.75, '-'],
               ['Person C', 2, 1.0, '-'],
               ['Person C', 3, 1.25, '-'],
               ['Person C', 4, 1.5, '-']]

portrait_09 = [['Person D', 3, 1.25, '-'],
               ['Person D', 1, 0.8, '-'],
               ['Person D', 0, 1.0, '-']]

portrait_10 = [['Pet', 1, 1.3, '-'],
               ['Pet', 2, 1.0, '-'],
               ['Pet', 3, 0.7, '-']]

# The following data sets each draw a family portrait with all
# individuals at their default sizes.  These data sets create
# "natural" looking portraits.  Notably, the first two both
# show the full family.

portrait_11 = [['Person A', 0, 1.0, '-'],
               ['Person B', 1, 1.0, '-'],
               ['Person C', 2, 1.0, '*'],
               ['Person D', 3, 1.0, '-'],
               ['Pet', 4, 1.0, '-']]

portrait_12 = [['Person A', 2, 1.0, '-'],
               ['Person B', 3, 1.0, '*'],
               ['Person C', 1, 1.0, '-'],
               ['Person D', 4, 1.0, '-'],
               ['Pet', 0, 1.0, '-']]

portrait_13 = [['Person B', 1, 1.0, '-'],
               ['Pet', 2, 1.0, '-'],
               ['Person C', 3, 1.0, '*']]

portrait_14 = [['Person C', 0, 1.0, '-'],
               ['Pet', 1, 1.0, '-'],
               ['Person A', 2, 1.0, '*'],
               ['Person D', 3, 1.0, '-'],
               ['Person B', 4, 1.0, '-']]

portrait_15 = [['Person D', 4, 1.0, '*'],
               ['Person A', 3, 1.0, '-'],
               ['Person B', 2, 1.0, '-']]

portrait_16 = [['Person D', 1, 1.0, '-'],
               ['Person C', 0, 1.0, '-'],
               ['Person A', 2, 1.0, '-'],
               ['Person B', 3, 1.0, '*']]

# The following data sets draw all five individuals at their
# minimum and maximum heights.

portrait_17 = [['Person A', 0, 0.5, '-'],
               ['Person B', 1, 0.5, '-'],
               ['Person C', 2, 0.5, '*'],
               ['Person D', 3, 0.5, '-'],
               ['Pet', 4, 0.5, '-']]

portrait_18 = [['Person A', 4, 1.5, '-'],
               ['Person B', 3, 1.5, '*'],
               ['Person C', 2, 1.5, '-'],
               ['Person D', 1, 1.5, '-'],
               ['Pet', 0, 1.5, '-']]

# The following data sets each draw a family portrait with
# various individuals at varying sizes.

portrait_19 = [['Person A', 0, 0.5, '*'],
               ['Person B', 1, 0.8, '-'],
               ['Person C', 2, 1.5, '-'],
               ['Person D', 3, 1.5, '-'],
               ['Pet', 4, 0.5, '-']]

portrait_20 = [['Person B', 1, 0.8, '*'],
               ['Pet', 2, 1.4, '-'],
               ['Person C', 3, 0.7, '-']]

portrait_21 = [['Person C', 0, 1.5, '-'],
               ['Pet', 1, 1.0, '-'],
               ['Person A', 2, 1.5, '-'],
               ['Person D', 3, 1.5, '*'],
               ['Person B', 4, 1.5, '-']]

portrait_22 = [['Person D', 4, 1.2, '-'],
               ['Person A', 3, 1.0, '*'],
               ['Person B', 2, 0.8, '-']]

portrait_23 = [['Person D', 1, 1.1, '-'],
               ['Person C', 2, 0.9, '-'],
               ['Person A', 0, 1.1, '*'],
               ['Person B', 3, 0.9, '-']]

# ***** If you want to create your own data sets you can add them here
# ***** (but your code must still work with all the data sets above plus
# ***** any other data sets in this style).

#
#--------------------------------------------------------------------#



#-----Student's Solution---------------------------------------------#
#
#  Complete the assignment by replacing the dummy function below with
#  your own "draw_portrait" function.
#

# Draw the stick figures as per the provided data set

#-----Individual figures---------------------------------------------#
#
#  This section creates the functions to draw each individual
#  figure; Dad, Mum, Billy, Sally and Wormy. Each figure consists
#  of an array of polar coordinates. These are passed to the
#  draw_shape, arms, head and foot functions which loop through
#  and draw the individual body parts.
#

def draw_shape(colour,coords_start,coords_stop):
    """draw_shape takes the x and y coordinates from a list of polar coordinates
    (or a slice of a list) and draws the lines between them to form a body shape.

    col provides a fill color and should be provided as either a string ('black'),
    rgb or hex# color.

    coords_start and coords_stop are the start and end index in the coordinate list.
    Note coords_stop should be the desired end position in the
    list +1 due to the function of range command"""
    
    fillcolor(colour)
    begin_fill()
    pendown()
    for coordinate_set in range(coords_start,coords_stop):
        goto(x_offset+coordinate_list[coordinate_set][0],
             coordinate_list[coordinate_set][1]*stretch_factor)
    end_fill()
    penup()
    
def draw_arms(colour,start_coord,size,line_width=5):
    """arms draws an arm and hand of the figure.
    start_coords should be the index position in the coordinate list
    of the highest point of the arm (at the sleeve).

    col is the "skin" color of the hand. Either a string, rgb or hex#
    can be used.

    line_width provides the line width of the hand."""
    
    #Draw the "stick"
    penup()
    goto(x_offset+coordinate_list[start_coord][0],
         coordinate_list[start_coord][1]*stretch_factor)
    pendown()
    goto(x_offset+coordinate_list[start_coord+1][0],
         coordinate_list[start_coord+1][1]*stretch_factor)
    #Set hand size and shape and stamp at the end of the stick
    shape('circle')
    shapesize(size*stretch_factor,size,line_width)
    fillcolor(colour)
    stamp()
    penup()
    
def draw_head(skin_colour,hair_colour,coords,head_size,line_width,hair_style):

    """head draws a head and face proportional to the desired size of the head.

    skin_colour is the skin colour of the face and hair_colour provides a hair color.
    Either a string, rgb or hex# can be used.

    coords is the index of the coordinates for the centre of the head.

    head_size is the vertical stretch of the oval used for the head. All elements of
    the face will be proportional to this value.

    line_width is the line thickness of the elipse used for the head.

    hair_style is the desired hair style of the figure. At this stage the options
    are 'afro' or 'mohawk'. If no hair style is desired enter '' for this value.

    If the data set indicates that a crown should be added to the figure, it is
    also added by this function"""
    #Note that all constants in this function are arbitrarly
    #decided constants of proportionality
    
    #Ensure pen is up and heading is set to 0
    penup()
    setheading(0)
    #Select amd draw hair style
    if hair_style=='afro':
        #Draw and afro
        goto(coordinate_list[coords][0]+x_offset,
             coordinate_list[coords][1]*stretch_factor*1.05)
        fillcolor(hair_colour)
        shape("circle")
        shapesize(head_size*1.01*stretch_factor,head_size*1.01,line_width)
        stamp()
    elif hair_style=='mohawk':
        #Draw a mohawk
        goto(coordinate_list[coords][0]+x_offset-(head_size*2.8),
             coordinate_list[coords][1]*1.02*stretch_factor)
        pendown()
        fillcolor(hair_colour)
        begin_fill()
        goto(coordinate_list[coords][0]+x_offset+(head_size*2.8),
             coordinate_list[coords][1]*1.02*stretch_factor)
        goto(coordinate_list[coords][0]+x_offset+(head_size*2.8),
             coordinate_list[coords][1]*1.1*stretch_factor)
        goto(coordinate_list[coords][0]+x_offset,
             coordinate_list[coords][1]*1.2*stretch_factor)
        goto(coordinate_list[coords][0]+x_offset-(head_size*2.8),
             coordinate_list[coords][1]*1.1*stretch_factor)
        goto(coordinate_list[coords][0]+x_offset-(head_size*2.8),
             coordinate_list[coords][1]*1.02*stretch_factor)
        end_fill()
        penup()
    #Create main face elipse
    goto(coordinate_list[coords][0]+x_offset,
         coordinate_list[coords][1]*stretch_factor)
    shape("circle")
    shapesize(head_size*stretch_factor,head_size*0.8,line_width)
    fillcolor(skin_colour)
    stamp()
    #Create a nose using the classic turtle cursor pointed upwards
    shape("classic")
    shapesize((head_size/3)*stretch_factor,head_size/3,line_width*2/5)
    setheading(90)
    stamp()
    #Create eyes
    shape("circle") 
    shapesize(head_size/3.8,(head_size/6.25)*stretch_factor,1)
    fillcolor("snow") #the preceding three lines set the shape, size and colour of the eye
    goto(coordinate_list[coords][0]+x_offset-(head_size*2.8),
         coordinate_list[coords][1]*1.02*stretch_factor)#moves to the left eye position
    stamp()#stamps eyeball
    dot(head_size*2*stretch_factor,'black')#stamps a pupil
    goto(coordinate_list[coords][0]+x_offset+(head_size*2.8),
         coordinate_list[coords][1]*1.02*stretch_factor)#moves to the right eye position
    stamp()#stamps eyeball
    dot(head_size*2*stretch_factor,'black')#stamps a pupil
    #Draw a smile by transcribing an eliptical arc, angle is in radians
    goto(coordinate_list[coords][0]+x_offset,
         coordinate_list[coords][1]*0.97*stretch_factor)
    smile_centre=coordinate_list[coords][1]*0.97*stretch_factor
    angle=pi
    while angle<=2*pi:
        if angle==pi:
            penup()
        else:
            pendown()
        goto(coordinate_list[coords][0]+x_offset+(head_size*3.6*cos(angle)),
             smile_centre+(head_size*3.6*sin(angle))*stretch_factor)
        angle+=pi/10
    penup()
    #Check to see if the figure requires a crown and draws it.
    if crown==True:
       crown_coordinate_list=[[-13.0,0.0],[13.0,0.0],[20.0,23.0],[13.0,20.0],
                              [10.0,35.0],[5.0,25.0 ],[0.0,40.0],[-5.0 ,25.0 ],
                              [-10.0,35.0],[-13.0,20.0],[-20.0,23.0],[-13.0,0.0],
                              [0.0,10]]
       setheading(0)
       width(2)
       penup()
       #Draw the crown shape
       goto(x_offset+crown_coordinate_list[0][0],(coordinate_list[coords][1]*1.05+crown_coordinate_list[0][1])*stretch_factor)
       fillcolor('gold')
       pendown()
       begin_fill()
       for coordinate_set in range(1,12):
           goto(x_offset+crown_coordinate_list[coordinate_set][0],
                (coordinate_list[coords][1]*1.05+crown_coordinate_list[coordinate_set][1])*stretch_factor)
       end_fill()
       #Draw the jewels
       penup()
       goto(x_offset+crown_coordinate_list[12][0],
            (coordinate_list[coords][1]*1.05+crown_coordinate_list[12][1])*stretch_factor)
       shape('circle')
       shapesize(0.6*stretch_factor,0.5,1)
       fillcolor('red2')
       stamp()
       shapesize(0.3*stretch_factor,0.25,1)
       fillcolor('blue2')
       goto(x_offset+crown_coordinate_list[2][0],
            (coordinate_list[coords][1]*1.05+crown_coordinate_list[2][1])*stretch_factor)
       stamp()
       fillcolor('red2')
       goto(x_offset+crown_coordinate_list[4][0],
            (coordinate_list[coords][1]*1.05+crown_coordinate_list[4][1])*stretch_factor)
       stamp()
       fillcolor('green2')
       goto(x_offset+crown_coordinate_list[6][0],
            (coordinate_list[coords][1]*1.05+crown_coordinate_list[6][1])*stretch_factor)
       stamp()
       fillcolor('red2')
       goto(x_offset+crown_coordinate_list[8][0],
            (coordinate_list[coords][1]*1.05+crown_coordinate_list[8][1])*stretch_factor)
       stamp()
       fillcolor('blue2')
       goto(x_offset+crown_coordinate_list[10][0],
            (coordinate_list[coords][1]*1.05+crown_coordinate_list[10][1])*stretch_factor)
       stamp()

def draw_foot(colour,coords,size):
    """foot draws a shoe for the figure.

    colour is the desired shoe colour. Either a string, rgb or hex# can be used.

    coords is the index of the postion coordinates for the centre of the shoe.

    size is the radius of the arc used to draw the shoe."""
    penup()
    goto(x_offset+coordinate_list[coords][0],coordinate_list[coords][1]*stretch_factor)
    fillcolor(colour)
    pendown()
    begin_fill()
    angle=pi
    while angle>=0:
        goto(x_offset+coordinate_list[coords][0]+(size*cos(angle)),
             (coordinate_list[coords][1]*stretch_factor)+(size*sin(angle))*stretch_factor)
        angle+=-pi/10
    goto(x_offset+coordinate_list[coords][0],coordinate_list[coords][1]*stretch_factor)
    end_fill()
    penup()

def dad():
    """dad draws the Dad figure. The coordinate_list list contains the coordinates of all needed points
    to be fed to the drawing functions."""
    #Initialize global variables and ensure line width and heading are correct
    #and pen is up.
    global coordinate_list
    width(5)
    setheading(0)
    penup()

    #coordinate_list is the list of coordinates for Dad's body parts
    coordinate_list=[[15.0,160.0],[26.0,240.0],[31.0,220.0],[44.0,236.0],[31.0,256.0],
                     [27.0,260.0],[-27.0,260.0],[-31.0,256.0],[-44.0,236.0],
                     [-31.0,220.0],[-26.0,240.0],[-13.0,160.0],[13.0,160.0],
                     [-26.0,0.0],[-5.0,0.0],[0.0,115.0],[5.0,0.0],[26.0,0.0],
                     [15.0,160.0],[-15.0,160.0],[-26.0,0.0],[-37.5,228.0],
                     [-55.0,167.0],[37.5,228],[55.0,167],[0.0,285],[-16,0],
                     [16,0]]
    
    #Draw Dad's Shirt
    goto(x_offset+coordinate_list[0][0],coordinate_list[0][1]*stretch_factor)
    pendown()
    draw_shape("red",1,13) 

    #Draw Dad's Trousers
    penup()
    goto(x_offset+coordinate_list[13][0] ,coordinate_list[13][1]*stretch_factor)
    draw_shape("blue",14,21)
    
    #Draw Dad's arms
    draw_arms('sienna1',21,0.5)
    draw_arms('sienna1',23,0.5)

    #Draw Dad's head
    draw_head('sienna1','black',25,2.5,5,'afro')

    #Draw Dad's shoes
    draw_foot("black",26,8)
    draw_foot("black",27,8)

def mum():
    """mum draws the Mum figure. the coordinate_list list contains the coordinates of all needed points
    to be fed to the drawing functions."""

    #Initialize global variables and ensure line width and heading are correct
    #and pen is up.
    global coordinate_list
    width(5)
    setheading(0)
    penup()
    
    #coordinate_list is the list of coordinates for Mum's body parts
    coordinate_list=[[0,10],[-45.0,10.0],[-20.0,156.0],[-26.0,206.0],
                     [-33.0,190.0],[-45.0,196.0],[-31.0,223.0],[-26.0,228.0],
                     [26.0,228.0],[31.0,223.0],[45.0,195.0],[33.0,190.0],
                     [26.0,206.0],[20.0,156.0],[45.0,10.0],[0.0,10.0],[-39.0,193],
                     [-51.0,153.0],[39.0,193],[51.0,153],[0.0,255.0],[-25.0,10.0],
                     [-27.0,0.0],[25.0,10.0],[27.0,0.0]]
    
    #Draw Mum's body.
    goto(x_offset+coordinate_list[0][0],coordinate_list[0][1]*stretch_factor)
    draw_shape('goldenrod1',1,16)

    #Draw a zipper for Mum's dress.
    zipper_start=223*stretch_factor
    while zipper_start>=156*stretch_factor:
        goto(x_offset,zipper_start)
        dot(3)
        zipper_start+=-3*stretch_factor

    #Draw Mum's arms
    draw_arms('bisque2',16,0.4)
    draw_arms('bisque2',18,0.4)    

    #Draw Mum's head
    draw_head('bisque2','light goldenrod',20,2.25,5,'afro') 

    #Draw Mum's legs and shoes
    goto(x_offset+coordinate_list[21][0],coordinate_list[21][1]*stretch_factor)
    pendown()
    goto(x_offset+coordinate_list[22][0],coordinate_list[22][1]*stretch_factor)
    penup()
    goto(x_offset+coordinate_list[23][0],coordinate_list[23][1]*stretch_factor)
    pendown()
    goto(x_offset+coordinate_list[24][0],coordinate_list[24][1]*stretch_factor)
    width(3)
    draw_foot('red',22,6)
    draw_foot('red',24,6)

def billy():
    """billy draws the Billy figure. The coordinate_list list contains the coordinates of all needed points
    to be fed to the drawing functions."""

    #Initialize global variables and ensure line width and heading are correct
    #and pen is up.
    global coordinate_list
    width(3)
    setheading(0)
    penup()
    
    #coordinate_list is the list of coordinates for Billy's body parts
    coordinate_list=[[ -27.0 , 47.0 ],[ -7.0 , 47.0 ],[ 0.0 , 80.0 ],[ 7.0 , 47.0 ],
                     [ 27.0 , 47.0 ],[ 20.0 , 120.0 ],[ -20.0 , 120.0 ],
                     [ -27.0 , 47.0 ],[ 20.0 , 120.0 ],[ 20.0 , 165.0 ],
                     [ 37.0 , 145.0 ],[ 45.0 , 151.0 ],[ 30.0 , 185.0 ],
                     [ 17.0 , 190.0 ],[ -18.0 , 190.0 ],[ -30.0 , 185.0 ],
                     [ -45.0 , 151.0 ],[ -37.0 , 145.0 ],[ -20.0 , 165.0 ],
                     [ -20.0 , 120.0 ],[ 20.0 , 120.0 ],[ 0.0 , 210.0 ],
                     [ -41.0 , 148.0 ],[ -51.4 , 130.0 ],[ 41.0 , 148.0 ],
                     [  51.4, 130.0 ],[ -17.0 , 47.0 ],[ -20.0 , 0.0 ],
                     [ 17.0 , 47.0 ],[ 20.0 , 0.0 ]]

    #Draw Billy's shorts
    goto(x_offset+coordinate_list[0][0],coordinate_list[0][1]*stretch_factor)
    draw_shape("navy",1,8)
    
    #Draw Billy's shirt
    goto(x_offset+coordinate_list[8][0],coordinate_list[8][1]*stretch_factor)
    draw_shape("gray15",9,21)

    #Draw billy's head
    draw_head('burlywood3',"dark orchid",21,1.75,3,'mohawk')
   
    #Draw Billy's arms
    draw_arms('burlywood3',22,0.45,3)
    draw_arms('burlywood3',24,0.45,3)    

    #Draw Billy's legs
    goto(x_offset+coordinate_list[26][0],coordinate_list[26][1]*stretch_factor)
    pendown()
    goto(x_offset+coordinate_list[27][0],coordinate_list[27][1]*stretch_factor)
    penup()
    goto(x_offset+coordinate_list[28][0],coordinate_list[28][1]*stretch_factor)
    pendown()
    goto(x_offset+coordinate_list[29][0],coordinate_list[29][1]*stretch_factor)

    #Draw Billy's shoes
    draw_foot("tomato",27,6)
    draw_foot("tomato",29,6)

def sally():
    """sally draws the Sally figure. The coordinate_list list contains the coordinates of all needed points
    to be fed to the drawing functions."""

    #Initialize global variables and ensure line width and heading are correct
    #and pen is up.
    global coordinate_list
    width(3)
    setheading(0)
    penup()
    
    coordinate_list=[[-38.0,35.0],[38.0,35.0],[14.0,75.0],[-14.0,75.0],[-38.0,35.0],
                     [14.0,75.0],[14.0,125.0],[19.0,120.0],[23.0,127.0],[13.0,135.0],
                     [0.0,134.0],[-13.0,135.0],[-23.0,126.0],[-19.0,120.0],[-14.0,125.0],
                     [-14.0,75.0],[14.0,75.0],[0.0,150.0],[-21.0,123.0],[-33.0,100.0],
                     [21.0,123.0],[33.0,100.0],[-17.0,35.0],[-19.0,0.0],[17.0,35.0],
                     [19.0,0.0]]

    #Draw Sally's skirt
    goto(x_offset+coordinate_list[0][0],coordinate_list[0][1]*stretch_factor)
    draw_shape('pink',1,5)

    #Draw Sally's top
    goto(x_offset+coordinate_list[5][0],coordinate_list[5][1]*stretch_factor)
    draw_shape('pink',6,17)

    #Draw Sally's head
    draw_head('peach puff','brown4',17,1.5,3,'afro')

    #Draw Sally's Arms
    draw_arms('peach puff',18,0.4,3)
    draw_arms('peach puff',20,0.4,3)

    #Draw Sally's Legs
    goto(x_offset+coordinate_list[22][0],coordinate_list[22][1]*stretch_factor)
    pendown()
    goto(x_offset+coordinate_list[23][0],coordinate_list[23][1]*stretch_factor)
    penup()
    goto(x_offset+coordinate_list[24][0],coordinate_list[24][1]*stretch_factor)
    pendown()
    goto(x_offset+coordinate_list[25][0],coordinate_list[25][1]*stretch_factor)

    #Draw Sally's shoes
    draw_foot('pink',23,4)
    draw_foot('pink',25,4)

def wormy():
    """wormy draws the Wormy figure. The coordinate_list list contains the coordinates for the centre of the head.
    Wormy's body is drawn as a sine curve and as such, no other functions are used"""
    #Setup global variables and set the turtle shape for Wormy's body sections.
    global coordinate_list
    global n
    shape('circle')
    shapesize(1*stretch_factor,1,1)
    fillcolor('slate blue')
    #Draw Wormy's body as a sin curve using a while loop
    n=0
    while n>=-3.2*pi:
        goto(x_offset-20+n*-5,20+sin(n)*10*stretch_factor)
        stamp()
        n+=-pi/10
    coordinate_list=[[],[-20+n*-5,20+sin(n)*10*stretch_factor]]
    #Draw wormy's antennae.
    shapesize(0.5)
    pendown()
    goto(x_offset-20+n*-5-20,20+sin(n)*10*stretch_factor+20)
    stamp()
    penup()
    goto(x_offset-20+n*-5,20+sin(n)*10*stretch_factor)
    pendown()
    goto(x_offset-20+n*-5+20,20+sin(n)*10*stretch_factor+20)
    stamp()
    #Draw wormy's head
    draw_head('slate blue','slate blue',1,1.5,1,'nohair')

def draw_portrait(port_name):
    """draw_portrait draws the portrait from the values contained in the list provided by the port_name argument"""
    #Initialize global variables
    global position, x_offset, stretch_factor, crown

    #Iterate through the portrait data set
    for figure in range(len(port_name)):
        #Identify postion for the current figure
        position=port_name[figure][1]
        if position==0:
            x_offset=-300
        elif position==1:
            x_offset=-150
        elif position==2:
            x_offset=0
        elif position==3:
            x_offset=150
        elif position==4:
            x_offset=300
        #Set the vertical scale factor
        stretch_factor=1+(port_name[figure][2]-1)/2
        #Check to see if the current figure will be crowned
        if port_name[figure][3]=='*':
            crown=True
        else:
            crown=False
        #Identify which figure to use and draw it
        if port_name[figure][0]=='Person A':
            dad()
        elif port_name[figure][0]=='Person B':
            mum()
        elif port_name[figure][0]=='Person C':
            billy()
        elif port_name[figure][0]=='Person D':
            sally()
        elif port_name[figure][0]=='Pet':
            wormy()

#
#--------------------------------------------------------------------#



#-----Main Program---------------------------------------------------#
#
# This main program sets up the background, ready for you to start
# drawing your stick figures.  Do not change any of this code except
# where indicated by comments marked '*****'.
#
    
# Set up the drawing window with a blue background representing
# the sky, and with the "home" coordinate set to the middle of the
# area where the stick figures will stand
setup(window_width, window_height)
setworldcoordinates(-window_width / 2, grass_offset,
                    window_width / 2, window_height + grass_offset)
bgcolor('sky blue')

# Draw the grass (with animation turned off to make it faster)
tracer(False)
draw_grass()

# Give the window a title
# ***** Replace this title with one that describes your choice
# ***** of individuals
title('My Proportionately Faced Family and Wormy')

# Control the drawing speed
# ***** Modify the following argument if you want to adjust
# ***** the drawing speed
speed('fastest')

# Draw the locations to stand, their labels and selected coordinates
# ***** If you don't want to display these background elements,
# ***** to make your portrait look nicer, change the corresponding
# ***** argument(s) below to False
draw_locations(True)
draw_labels(True)
mark_coords(True)

# Call the student's function to display the stick figures
# ***** If you want to turn off animation while drawing your
# ***** stick figures, to make your program draw faster, change
# ***** the following argument to False
tracer(True)
# ***** Change the argument to this function to test your
# ***** code with different data sets
draw_portrait(portrait_00)

# Exit gracefully by hiding the cursor and releasing the window
tracer(True)
hideturtle()
done()

#
#--------------------------------------------------------------------#

