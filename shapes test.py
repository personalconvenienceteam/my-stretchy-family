#shapes file

from turtle import *
from math import *

setup()
x_offset=0
stretch_factor=1
xy=[[-13.0,0.0],[13.0,0.0],[20.0,23.0],[13.0,20.0],[10.0,35.0],
    [5.0,25.0 ],[0.0,40.0],[-5.0 ,25.0 ],[-10.0,35.0],[-13.0,20.0],
    [-20.0,23.0],[-13.0,0.0],[0.0,10]]
setheading(0)
penup()
goto(xy[0])
fillcolor('gold')
pendown()
begin_fill()
for i in range(1,12):
    goto(x_offset+xy[i][0],xy[i][1]*stretch_factor)
end_fill()
penup()
goto(x_offset+xy[12][0],xy[12][1]*stretch_factor)
shape('circle')
shapesize(0.6*stretch_factor,0.5,1)
fillcolor('red2')
stamp()
shapesize(0.3*stretch_factor,0.25,1)
fillcolor('blue2')
goto(x_offset+xy[2][0],xy[2][1]*stretch_factor)
stamp()
fillcolor('red2')
goto(x_offset+xy[4][0],xy[4][1]*stretch_factor)
stamp()
fillcolor('green2')
goto(x_offset+xy[6][0],xy[6][1]*stretch_factor)
stamp()
fillcolor('red2')
goto(x_offset+xy[8][0],xy[8][1]*stretch_factor)
stamp()
fillcolor('blue2')
goto(x_offset+xy[10][0],xy[10][1]*stretch_factor)
stamp()
